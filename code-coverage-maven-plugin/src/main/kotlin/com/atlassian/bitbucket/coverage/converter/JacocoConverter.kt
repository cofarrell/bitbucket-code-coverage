package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.XmlReader
import com.atlassian.bitbucket.coverage.jacoco.Counter
import com.atlassian.bitbucket.coverage.jacoco.Package
import com.atlassian.bitbucket.coverage.jacoco.Report
import com.atlassian.bitbucket.coverage.jacoco.Sourcefile
import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path

class JacocoConverter: AbstractCoverageConverter() {
    companion object {
        const val BRANCH_COUNTER = "COMPLEXITY"
        const val INSTRUCTION_COUNTER = "INSTRUCTION"

        val logger: Logger = LoggerFactory.getLogger(JacocoConverter.javaClass)
    }

    override fun convert(coverageFile: File, projectPath: Path): CommitCoverageEntity {
        val report: Report = XmlReader().read(Report::class.java, coverageFile)
        val sourceFileResolver = SourceFileResolver(projectPath)

        val files = mutableListOf<FileCoverageEntity?>()

        for (pkg in report.groupOrPackage) {
            if (pkg is Package) {
                logger.debug("Process package {}", pkg.name)
                val packageCoverage: List<FileCoverageEntity?> = convertPackage(pkg, sourceFileResolver)
                files.addAll(packageCoverage)
            } else {
                logger.warn("Unsuported entry type: {}", pkg.javaClass)
            }
        }

        val result = CommitCoverageEntity()
        result.files = files.filterNotNull()
        return result
    }

    fun convertPackage(pkg: Package, sourceFileResolver: SourceFileResolver): List<FileCoverageEntity?> {
        val result: MutableList<FileCoverageEntity?> = mutableListOf()

        for (clazzOrSourceFile in pkg.clazzOrSourcefile) {
            when (clazzOrSourceFile) {
                is com.atlassian.bitbucket.coverage.jacoco.Class -> {
                    val path = sourceFileResolver.resolveClass(clazzOrSourceFile.name)
                    val clazzCoverage = convertClass(path, clazzOrSourceFile)
                    result.add(clazzCoverage)
                }

                is Sourcefile -> {
                    val path = sourceFileResolver.resolveSource(pkg.name, clazzOrSourceFile.name)
                    val sourceCoverage = convertSourceFile(path, clazzOrSourceFile)
                    result.add(sourceCoverage)
                }
            }
        }

        return result
    }

    fun convertClass(path: String, clazz: com.atlassian.bitbucket.coverage.jacoco.Class): FileCoverageEntity? {
        val fileCoverageMap = emptyCoverageMap()

        for (method in clazz.method) {
            val lineNumber = method.line ?: continue

            val (coveredBranches, missedBranches) = getCoverage(BRANCH_COUNTER, method.counter)
            val (_, missedInstructions) = getCoverage(INSTRUCTION_COUNTER, method.counter)
            val coverageType = getCoverageState(missedBranches, coveredBranches, missedInstructions)
            fileCoverageMap[coverageType]!!.add(lineNumber)
        }

        return convertFileCoverage(path, fileCoverageMap)
    }

    fun convertSourceFile(path: String, sourcefile: Sourcefile): FileCoverageEntity? {
        val fileCoverageMap = emptyCoverageMap()

        for (line in sourcefile.line) {
            val coverageType = getCoverageState(line.mb, line.cb, line.mi)
            fileCoverageMap[coverageType]!!.add(line.nr)
        }

        return convertFileCoverage(path, fileCoverageMap)
    }

    fun getCoverage(type: String, counters: List<Counter>): Pair<String, String> {
        for (counter in counters) {
            if (counter.type.equals(type, ignoreCase = true)) {
                return Pair(counter.covered, counter.missed)
            }
        }
        throw IllegalArgumentException("Unknown coverage type: $type")
    }

    fun getCoverageState(missedBranches: String, coveredBranches: String, missedInstructions: String): String {
        return if (isSimpleStatement(missedBranches, coveredBranches)) {
            getLineCoverageState(missedInstructions)
        } else {
            getBlockCoverageState(missedBranches, coveredBranches)
        }
    }

    fun isSimpleStatement(missedBranches: String, coveredBranches: String): Boolean =
            missedBranches == "0" && coveredBranches == "0"

    fun getLineCoverageState(missedInstructions: String): String {
        return if (missedInstructions == "0") {
            COVERED_KEY
        } else {
            UNCOVERED_KEY
        }
    }

    fun getBlockCoverageState(missedBranches: String, coveredBranches: String): String {
        return when {
            missedBranches == "0" -> COVERED_KEY
            coveredBranches == "0" -> UNCOVERED_KEY
            else -> PARTLY_COVERED_KEY
        }
    }

}
