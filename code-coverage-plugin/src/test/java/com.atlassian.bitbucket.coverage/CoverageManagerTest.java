package com.atlassian.bitbucket.coverage;

import com.atlassian.bitbucket.coverage.dao.AoFileCoverage;
import com.atlassian.bitbucket.coverage.dao.CodeCoverageDao;
import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity;
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity;
import com.atlassian.bitbucket.transaction.SimpleSalTransactionTemplate;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CoverageManagerTest {
    private static final String COMMIT_ID1 = "asdfgh";
    private static final String PATH1 = "/file/path/1";
    private static final String PATH2 = "/file/path/3";
    private static final String COVERED1 = "1";
    private static final String PARTLY1 = "2";
    private static final String UNCOVERED1 = "3,4";
    private static final String COVERED2 = "1,2";

    @Mock
    private CodeCoverageDao codeCoverageDao;
    @Mock
    private AoFileCoverage fc1, fc2, fc3;
    @Mock
    private CoverageParser coverageParser;
    @Spy
    private TransactionTemplate transactionTemplate = new SimpleSalTransactionTemplate();
    @InjectMocks
    private CoverageManager coverageManager;

    private List<AoFileCoverage> fileCoverageList = new LinkedList<>();

    @Before
    public void setUp() {
        when(fc1.getPath()).thenReturn(PATH1);
        when(fc1.getCovered()).thenReturn(COVERED1);
        when(fc1.getPartlyCovered()).thenReturn(PARTLY1);
        when(fc1.getUnCovered()).thenReturn(UNCOVERED1);

        when(fc2.getPath()).thenReturn(PATH1);
        when(fc2.getCovered()).thenReturn(COVERED2);


        when(fc3.getPath()).thenReturn(PATH2);
        when(fc3.getCovered()).thenReturn(COVERED1);
        when(fc3.getPartlyCovered()).thenReturn(PARTLY1);
        when(fc3.getUnCovered()).thenReturn(UNCOVERED1);

        fileCoverageList.add(fc1);
        fileCoverageList.add(fc2);
        fileCoverageList.add(fc3);

        when(coverageParser.isValidCoverage(anyString())).thenReturn(true);
    }

    @Test
    public void entityFromCoverageReturnsNullForNullCoverage() {
        FileCoverageEntity result = coverageManager.entityFromCoverage((AoFileCoverage) null);

        assertThat(result, nullValue());
    }

    @Test
    public void entityFromCoverageReturnsNullForNullCoverageList() {
        List<FileCoverageEntity> result = coverageManager.entityFromCoverage((List<AoFileCoverage>)null);

        assertThat(result, nullValue());
    }

    @Test
    public void entityFromCoverageReturnsEntity() {
        FileCoverageEntity result = coverageManager.entityFromCoverage(fc1);

        assertThat(result, notNullValue());
    }

    @Test
    public void entityFromCoverageListReturnsList() {
        List<FileCoverageEntity> result = coverageManager.entityFromCoverage(fileCoverageList);

        assertThat(result, hasSize(fileCoverageList.size()));
    }

    @Test(expected = NullPointerException.class)
    public void saveCoverageThrowsNpe() throws InvalidCoverageException {
        coverageManager.saveCoverage(COMMIT_ID1,null);
    }

    @Test
    public void saveCoverageDoesNotCreateAnyObjectsWhenCoverageListIsNull() {
        CommitCoverageEntity entity = new CommitCoverageEntity();

        coverageManager.saveCoverage(COMMIT_ID1, entity);

        verifyZeroInteractions(codeCoverageDao);
    }

    @Test
    public void saveCoverageDoesNotCreateAnyObjectsWhenCoverageListIsEmpty() {
        CommitCoverageEntity entity = new CommitCoverageEntity();
        entity.setFiles(new ArrayList<>());

        coverageManager.saveCoverage(COMMIT_ID1, entity);

        verifyZeroInteractions(codeCoverageDao);
    }

    @Test
    public void saveCoverageForEveryFile() {
        CommitCoverageEntity entity = new CommitCoverageEntity();
        List<FileCoverageEntity> fileCoverageEntityList = new ArrayList<>(2);
        FileCoverageEntity fileCoverageEntity1 = coverageManager.entityFromCoverage(fc1);
        FileCoverageEntity fileCoverageEntity2 = coverageManager.entityFromCoverage(fc2);
        fileCoverageEntityList.add(fileCoverageEntity1);
        fileCoverageEntityList.add(fileCoverageEntity2);
        entity.setFiles(fileCoverageEntityList);

        coverageManager.saveCoverage(COMMIT_ID1, entity);

        verify(codeCoverageDao, times(2)).addFileCoverage(anyString(), anyString(), anyString());
    }

    @Test(expected = InvalidCoverageException.class)
    public void saveCoverageThrowsExceptionForInvalidEntries() {
        CommitCoverageEntity entity = new CommitCoverageEntity();
        List<FileCoverageEntity> fileCoverageEntityList = new ArrayList<>(2);
        FileCoverageEntity fileCoverageEntity1 = coverageManager.entityFromCoverage(fc1);
        FileCoverageEntity fileCoverageEntity2 = new FileCoverageEntity();
        fileCoverageEntity2.setPath(PATH2);
        fileCoverageEntity2.setCoverage(null);
        fileCoverageEntityList.add(fileCoverageEntity1);
        fileCoverageEntityList.add(fileCoverageEntity2);
        entity.setFiles(fileCoverageEntityList);

        coverageManager.saveCoverage(COMMIT_ID1, entity);
    }

    @Test
    public void getCoverageReturnsNullWhenNoMatchesFound() {
        when(codeCoverageDao.findFileCoverage(anyString(), anyString())).thenReturn(null);

        FileCoverageEntity result = coverageManager.getCoverage(COMMIT_ID1, PATH2);

        assertNull(result);
    }

}
