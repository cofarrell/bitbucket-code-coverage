package com.atlassian.bitbucket.coverage;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.PARTLY_COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.UNCOVERED_COLUMN;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class CoverageMergerTest {
    private static final String NEW_COVERAGE = "C:1,3,5,7;P:2,3,6;U:4,9";
    private static final String OLD_COVERAGE = "C:1,2;P:3,4;U:5,6,8";

    private CoverageMerger coverageMerger = new CoverageMerger();
    private CoverageParser coverageParser = new CoverageParser();

    private Map<String, Set<String>> oldCoverage = coverageParser.parseCoverage(OLD_COVERAGE);
    private Map<String, Set<String>> newCoverage = coverageParser.parseCoverage(NEW_COVERAGE);

    private Map<String, Set<String>> resultCoverage;

    @Before
    public void performMerge() {
        resultCoverage = coverageMerger.mergeCoverage(oldCoverage, newCoverage);
    }

    @Test
    public void mergeCoverageMergesCoverage() {
        Set<String> result = coverageMerger.mergeCoverage(
                newHashSet("1","2","3"), newHashSet("3","4"));

        assertThat(result, hasSize(4));
    }

    @Test
    public void mergeRemovesCoveredLinesFromPartlyCovered() {
        assertThat("3", not(isIn(resultCoverage.get(PARTLY_COVERED_COLUMN))));
    }

    @Test
    public void mergeRemovesCoveredLinesFromUncovered() {
        assertThat("5", not(isIn(resultCoverage.get(UNCOVERED_COLUMN))));
    }

    @Test
    public void mergeRemovesPartlyCoveredLinesFromUncovered() {
        assertThat("6", not(isIn(resultCoverage.get(UNCOVERED_COLUMN))));
    }

    @Test
    public void mergeJoinsCovered() {
        assertThat(resultCoverage.get(COVERED_COLUMN), contains("1", "2", "3", "5", "7"));
        assertThat(resultCoverage.get(COVERED_COLUMN), hasSize(5));
    }

    @Test
    public void mergeJoinsPartlyCovered() {
        assertThat(resultCoverage.get(PARTLY_COVERED_COLUMN), contains("4", "6"));
        assertThat(resultCoverage.get(PARTLY_COVERED_COLUMN), hasSize(2));
    }

    @Test
    public void mergeJoinsUncovered() {
        assertThat(resultCoverage.get(UNCOVERED_COLUMN), contains("8", "9"));
        assertThat(resultCoverage.get(UNCOVERED_COLUMN), hasSize(2));
    }

}
